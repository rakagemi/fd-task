import React from "react";
import Image from "next/image";
import Link from "next/link";
import styles from "../../styles/Footer.module.css";
import { Container, Row, Col } from "react-bootstrap";

const tfT = {
  data: [
    {
      id: 1,
      name: "About Us",
      desc: "Feedback",
      text: "Contact",
    },
    {
      id: 2,
      name: "Term & Conditions",
      desc: "Privacy Policy",
      text: "Help",
    },
    {
      id: 3,
      name: "Awards",
      desc: "Newsletter",
      text: "",
    },
  ],
};

const tfB = {
  data: [
    {
      id: 1,
      name: "Our HQ",
      desc: "Lantai 4 Wisma Prima, Jl. Kapten Tendean No.34, RT.2/RW.1, Mampang Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12720",
      href: "http://www.femaledaily.com/",
    },
    {
      id: 2,
      name: "Contact Us",
      desc: "hello@femaledaily.com",
      href: "mailto:hello@femaledaily.com",
    },
  ],
};

const tfL = {
  data: [
    {
      id: 1,
      alt: "Female Daily",
      src: "/image/footer-ig.svg",
      href: "https://www.instagram.com/femaledailynetwork"

    },
    {
      id: 2,
      alt: "Female Daily",
      src: "/image/footer-ig.svg",
      href: "https://www.instagram.com/femaledailynetwork"
    },
    {
      id: 3,
      alt: "Female Daily",
      src: "/image/footer-ig.svg",
      href: "https://www.instagram.com/femaledailynetwork"
 
    },
    {
      id: 3,
      alt: "Female Daily",
      src: "/image/footer-ig.svg",
      href: "https://www.instagram.com/femaledailynetwork"
    },
  ],
};

export default function Footer() {
  return (
    <>
      <footer className={styles.footerParent}>
        <div className={styles.footerPaddingParent}>
          <Container>
            <Row className={styles.row1footer}>
              {/* <Col md={3} className={styles.col1footer}>
                <h3 className={styles.tFT_estb}>ESTB Jakarta</h3>
                <h4 className={styles.dFT}>
                  Specializing in creating a data-based digital marketing
                  strategy for your business.
                </h4>
              </Col> */}
              {tfT.data?.length === 0 ? (
                <div></div>
              ) : (
                tfT.data?.map((datas) => (
                  <Col
                    key={datas.id.toString()}
                    md={3}
                    className={styles.col1footer}
                  >
                    <h3 style={{                  
                      fontSize: "12px",
                  color: "#000000",
                  fontWeight: "500",}}>{datas.name}</h3>
                    <h4 style={{                      fontSize: "12px",
                  color: "#000000",
                  fontWeight: "500",}}>{datas.desc}</h4>
                                      <h4 style={{                      fontSize: "12px",
                  color: "#000000",
                  fontWeight: "500",}}>{datas.text}</h4>
                  </Col>
                ))
              )}
              <Col className={styles.col1footer}>
                      <h3 style={{                  
                      fontSize: "12px",
                  color: "#000000",
                  fontWeight: "500",}}>Download Our Mobile Apps
                  </h3>
                  <Row>
                    <Col>
                    <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                    </Col>
                    <Col>
                    <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                    </Col>
                  </Row>
              </Col>
            </Row>
            <Row className={styles.row2footer}>
              <Col
                className={styles.col2footerImage}
                md={3}
                style={{ padding: "inherit" }}
              >
                <div style={{height: '70px'}}>
                <Image
                  alt="logo"
                  src={
                    "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                  }
                  width={100}
                  height={100}
                  quality="100%"
                  priority={true}
                  // className={styles.logoImage}
                />
                </div>
                <p style={{                  
                      fontSize: "12px",
                  color: "#000000",
                  fontWeight: "500",}}>Copyright © 2015 - 2023 Female Daily Network ∙ All the rights reserved.</p>
                {/* <div className={styles.col2footerImage}></div> */}
              </Col>
              {tfB.data?.length === 0 ? (
                <div></div>
              ) : (
                tfB.data?.map((datas) => (
                  <Col
                    key={datas.id.toString()}
                    md={3}
                    className={styles.col2footer}
                  >
                    <h3 className={styles.tFT}>{datas.name}</h3>
                    <Link
                      target="_blank"
                      rel="noopener noreferrer"
                      className={styles.dTF}
                      href={datas.href}
                    >
                      <h4 className={styles.dFT}>{datas.desc}</h4>
                    </Link>
                  </Col>
                ))
              )}
              <Col className={styles.col2footer} md={3}>
                <h3 className={styles.tFT}>Connect</h3>
                <Row>
                  {tfL.data?.length === 0 ? (
                    <div></div>
                  ) : (
                    tfL.data?.map((datas) => (
                      <Col
                        md={4}
                        key={datas.id.toString()}
                        style={{ width: "auto" }}
                      >
                        <Link
                          target="_blank"
                          rel="noopener noreferrer"
                          href={datas.href}
                        >
                          <Image
                            alt={datas.alt}
                            src={datas.src}
                            width={0}
                            height={0}
                            quality="100%"
                            priority={true}
                            className={styles.logoImageConnect}
                          />
                        </Link>
                      </Col>
                    ))
                  )}
                </Row>
              </Col>
            </Row>
            <Row>
              <Col               style={{
                justifyContent: "center",
                display: "flex",
              
              }} md={12}>
                <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "970px",
                  height: "50px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                }}
              >
                <p>Top Frame 970x50,  460x60, 320x50</p>
              </div>
              </Col>
              <Col               style={{
                justifyContent: "center",
                display: "flex",
               
              }} md={12}>
                <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "460px",
                  height: "60px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                }}
              >
                <p>Top Frame 460x60</p>
              </div>
              </Col>
              <Col               style={{
                justifyContent: "center",
                display: "flex",
               
              }} md={12}>
                <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "320px",
                  height: "50px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                }}
              >
                <p>Top Frame 320x50</p>
              </div>
              </Col>
            </Row>
          </Container>
        </div>
      </footer>
    </>
  );
}
