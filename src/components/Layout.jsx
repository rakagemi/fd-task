import React from "react";
import Navbars from "./Navbar/index";
import Footer from "./Footer";


export default function Layout({ children }) {
  return (
    <>
      <Navbars />
      <main style={{ backgroundColor: "#FFFFFF !important" }}>{children}</main>
      <Footer />
    </>
  );
}
