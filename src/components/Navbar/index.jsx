import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import styles from "../../styles/Navbar.module.css";
import {
  Container,
  Navbar,
  Nav,
  NavDropdown,
  Stack,
  Row,
  Modal,
  Form,
  Button,
} from "react-bootstrap";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { useRouter } from "next/router";
// import FormContactUs from "../Modal/ContactUs";

export default function Navbars() {
  const router = useRouter();
  const [navbar, setNavbar] = useState(false);

  const [bg, setBg] = useState("navbarParent");

  function toggleBackground() {
    if (bg === "navbarParent") setBg("navbarParentTransperant");
    if (bg === "navbarParentTransperant") setBg("navbarParent");
  }

  const changeBackground = () => {
    if (window.scrollY >= 80) {
      setNavbar(true);
    } else {
      setNavbar(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", changeBackground);
  }, []);

  const [modalShow, setModalShow] = useState(false);

  // const [show, setShow] = useState(false);

  // const handleClose = () => setShow(false);
  // const handleShow = () => setShow(true);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Form was submitted, now the modal can be closed");
    handleClose();
  };

  return (
    <>
      <Navbar
        expand="xl"
        sticky="top"
        className={
          navbar ? styles.navbarParent : styles.navbarParentTransperant
        }
      >
        <Container>
          <Navbar.Brand className={styles.navCollapse}>
            <Link href="/">
              <Image
                alt=" Logo"
                src={'https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg'}
                width={100}
                height={50}
                quality="100%"
                priority={true}
                className={styles.logoImage}
              />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="navbarScroll"
            // className={styles.navbar1Toggle}
          />
          <Navbar.Collapse
            className={styles.navbarCollapse1Bg}
            id="navbarScroll"
          >
            <Nav className={styles.navs1Bg} navbarScroll="true">
            <Form className="d-flex" style={{marginLeft: '1.5rem'}}>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            
          </Form>
          <Button style={{background: '#FF69B4', color: '#FFFFFF'}} variant="outline-success">Login / Signup</Button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Navbar
        expand="xl"
        sticky="top"
        className={
          navbar ? styles.navbarParent : styles.navbarParentTransperant
        }
      >
        <Container>
          <Navbar.Brand className={styles.navCollapse}>
            <Link href="/">
              <Image
                alt=" Logo"
                src={'https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg'}
                width={100}
                height={50}
                quality="100%"
                priority={true}
                className={styles.logoImage}
              />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle
            aria-controls="navbarScroll"
            // className={styles.navbar1Toggle}
          />
          <Navbar.Collapse
            className={styles.navbarCollapse1Bg}
            id="navbarScroll"
          >
            <Nav className={styles.navs1Bg} navbarScroll="true">
            <Form className="d-flex" style={{marginLeft: '1.5rem'}}>
            <Form.Control
              type="search"
              placeholder="Search Products"
              className="me-2"
              aria-label="Search"
            />
            
          </Form>
          <Button style={{background: '#FF69B4', color: '#FFFFFF'}} variant="outline-success">Login / Signup</Button>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
