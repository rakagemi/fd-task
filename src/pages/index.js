import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const inter = Inter({ subsets: ["latin"] });

export default function Home({ editor, article, review }) {
  const settingsSlider = {
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "350px",
    slidesToShow: 1,
    speed: 500,
    speedplaySpeed: 1000,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1930,
        settings: {
          slidesToShow: 3,
          infinite: true,
          dots: false,
          className: "center",
          centerMode: true,
          infinite: true,
          centerPadding: "100px",
        },
      },
      {
        breakpoint: 1680,
        settings: {
          slidesToShow: 1,
          infinite: true,
          dots: false,
          className: "center",
          centerMode: true,
          infinite: true,
          centerPadding: "400px",
        },
      },
      {
        breakpoint: 1440,
        settings: {
          className: "center",
          centerMode: false,
          infinite: true,
          centerPadding: "350px",
          slidesToShow: 2,
          speed: 500,
          speedplaySpeed: 1000,
          autoplay: true,
        },
      },
      {
        breakpoint: 1290,
        settings: {
          centerPadding: "300px",
          infinite: true,
          dots: false,
        },
      },
      {
        breakpoint: 430,
        settings: {
          centerMode: false,
          slidesToShow: 1,
          infinite: true,
          dots: false,
        },
      },
    ],
  };

  return (
    <>
      <Head>
        <title>FD TASK</title>
        <meta name="description" content="FD TASK" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
          rel="icon"
          href="https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
        />
      </Head>
      <main className={styles.main}>
        <Container>
          <Row>
            <Col
              md={12}
              style={{
                justifyContent: "center",
                display: "flex",
                marginBottom: "2rem",
              }}
            >
              <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "970px",
                  height: "50px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                }}
              >
                <p>Top Frame 970x50</p>
              </div>
            </Col>
            <Col md={12} style={{ justifyContent: "center", display: "flex" }}>
              <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "970px",
                  height: "250px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                }}
              >
                <p>Top Frame 970x250</p>
              </div>
            </Col>
          </Row>
          {/* S2 */}
          <Row style={{ padding: "2rem 4rem" }}>
            <Col md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Editors Choice
              </h2>
              <h3
                style={{
                  fontSize: "18px",
                  color: "grey",
                  fontWeight: "400",
                }}
              >
                Curated with love
              </h3>
            </Col>
            <Col md={12}>
              <Row>
                {editor?.length === 0 ? (
                  <div></div>
                ) : (
                  editor?.map((e) => (
                    <Col key={e.id}>
                      <Row style={{ marginTop: "1rem" }}>
                        <Col md={3}>
                          <Image
                            // className={styles.ImageArtc}
                            alt="article"
                            width={35}
                            height={35}
                            src={e.product.image}
                          />
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "14px",
                              fontWeight: "700",
                              marginBottom: "0",
                              color: "#000",
                            }}
                          >
                            {e.editor}
                          </p>
                          <p
                            style={{
                              fontSize: "14px",
                              fontWeight: "400",
                              color: "#000",
                            }}
                          >
                            {e.role}
                          </p>
                        </Col>
                      </Row>
                      <Card className={styles.CardArtc}>
                        <Image
                          className={styles.ImageArtc}
                          alt="article"
                          width={295}
                          height={418}
                          src={e.product.image}
                        />
                        <Card.Body className={styles.cardBodyArtc}>
                          <p
                            style={{
                              fontSize: "14px",
                              fontWeight: "700",
                              color: "#000",
                              "&:hover": {
                                color: "hotpink",
                              },
                            }}
                          >
                            {e.product.rating}
                          </p>
                          <p
                            style={{
                              fontSize: "18px",
                              fontWeight: "700",
                              color: "#000",
                              marginBottom: "0",
                            }}
                          >
                            {e.product.name}
                          </p>
                          <p
                            style={{
                              fontSize: "14px",
                              fontWeight: "500",
                              color: "#000",
                            }}
                          >
                            {e.product.description}
                          </p>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))
                )}
              </Row>
            </Col>
          </Row>
          {/* S3 */}
          <Row style={{ background: "bisque" }}>
            <Col md={5}>
              <Row>
                <Col md={4} style={{ display: "flex", alignItems: "center" }}>
                  <Image
                    src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                    alt="fd task"
                    width={100}
                    height={100}
                    className={styles.imgS3}
                  />
                </Col>
                <Col
                  md={7}
                  style={{
                    padding: "2rem 0 2rem 0",
                  }}
                >
                  <h2
                    style={{
                      fontSize: "22px",
                      fontWeight: "700",
                      color: "#000",
                    }}
                  >
                    Looking for products that are just simply perfect for you?
                  </h2>
                  <h2
                    style={{
                      fontSize: "18px",
                      fontWeight: "400",
                      color: "#000000",
                      "&:hover": {
                        color: "hotpink",
                      },
                    }}
                  >
                    Here some products that we believe match for your skin and
                    hair, body! Have we mentioned that they solved your concerns
                    too?
                  </h2>
                  <div style={{ display: "flex", justifyContent: "end" }}>
                    <Button
                      style={{
                        background: "salmon",
                        borderColor: "unset",
                        width: "150px",
                        fontWeight: "400",
                      }}
                    >
                      See My Match
                    </Button>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col>
              <Row>
                <Col style={{ padding: "1rem" }}>
                  <Card className={styles.CardArtc}>
                    <Image
                      className={styles.ImageArtc}
                      alt="article"
                      width={150}
                      height={150}
                      src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                    />
                    <Card.Body className={styles.cardBodyArtc}>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "700",
                          color: "#000",
                          "&:hover": {
                            color: "hotpink",
                          },
                        }}
                      >
                        4.1
                      </p>
                      <p
                        style={{
                          fontSize: "18px",
                          fontWeight: "700",
                          color: "#000",
                          marginBottom: "0",
                        }}
                      >
                        Y.O.U Makeups
                      </p>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "500",
                          color: "#000",
                        }}
                      >
                        Rouge Velvet Matte Lip Cream
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
                <Col style={{ padding: "1rem" }}>
                  <Card className={styles.CardArtc}>
                    <Image
                      className={styles.ImageArtc}
                      alt="article"
                      width={150}
                      height={150}
                      src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                    />
                    <Card.Body className={styles.cardBodyArtc}>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "700",
                          color: "#000",
                          "&:hover": {
                            color: "hotpink",
                          },
                        }}
                      >
                        4.1
                      </p>
                      <p
                        style={{
                          fontSize: "18px",
                          fontWeight: "700",
                          color: "#000",
                          marginBottom: "0",
                        }}
                      >
                        Y.O.U Makeups
                      </p>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "500",
                          color: "#000",
                        }}
                      >
                        Rouge Velvet Matte Lip Cream
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
                <Col style={{ padding: "1rem" }}>
                  <Card className={styles.CardArtc}>
                    <Image
                      className={styles.ImageArtc}
                      alt="article"
                      width={150}
                      height={150}
                      src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                    />
                    <Card.Body className={styles.cardBodyArtc}>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "700",
                          color: "#000",
                          "&:hover": {
                            color: "hotpink",
                          },
                        }}
                      >
                        4.1
                      </p>
                      <p
                        style={{
                          fontSize: "18px",
                          fontWeight: "700",
                          color: "#000",
                          marginBottom: "0",
                        }}
                      >
                        Y.O.U Makeups
                      </p>
                      <p
                        style={{
                          fontSize: "14px",
                          fontWeight: "500",
                          color: "#000",
                        }}
                      >
                        Rouge Velvet Matte Lip Cream
                      </p>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* SEC4 */}
          <Row style={{ padding: "2rem 0 2rem 0" }}>
            <Col
              md={12}
              style={{
                justifyContent: "center",
                display: "flex",
                marginBottom: "2rem",
              }}
            >
              <div
                style={{
                  background: "lightgrey",
                  color: "#00000",
                  border: "1px solid #000000",
                  width: "970px",
                  height: "250px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  fontSize: "28px",
                  textAlign: "center",
                }}
              >
                <p style={{ textAlign: "center" }}>
                  Top Frame 970x50 <br></br> (Internal campaign only)
                </p>
              </div>
            </Col>
          </Row>
          {/* SEC 5 */}
          <Row>
            <Col md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Latest Articles
              </h2>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "grey",
                    fontWeight: "400",
                  }}
                >
                  So you can make better purchase
                </h3>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "deeppink",
                    fontWeight: "400",
                  }}
                >
                  See more &#x3e;
                </h3>
              </div>
            </Col>
            <Col md={12} style={{ marginTop: "1rem" }}>
              <Row>
                {article?.length === 0 ? (
                  <div></div>
                ) : (
                  article?.map((e) => (
                    <Col style={{ marginBottom: "1rem" }} key={e.id} md={4}>
                      <Card style={{ borderRadius: "10px" }}>
                        <Image
                          className={styles.ImageArtc}
                          alt="article"
                          width={150}
                          height={150}
                          src={e.image}
                          style={{ borderRadius: "10px" }}
                        />
                        <Card.Body>
                          <p
                            style={{
                              fontSize: "18px",
                              fontWeight: "700",
                              color: "#000",
                              marginBottom: "1rem",
                            }}
                          >
                            {e.title}
                          </p>
                          <div style={{ display: "flex" }}>
                            <p
                              style={{
                                fontSize: "16px",
                                fontWeight: "500",
                                color: "#000",
                                marginBottom: "0",
                                marginRight: "0.5rem",
                              }}
                            >
                              {e.author}
                            </p>
                            <p
                              style={{
                                marginBottom: "0",
                                marginRight: "0.5rem",
                              }}
                            >
                              |
                            </p>
                            <p
                              style={{
                                fontSize: "14px",
                                fontWeight: "400",
                                color: "#000",
                                marginBottom: "0",
                              }}
                            >
                              {e.published_at}
                            </p>
                          </div>
                        </Card.Body>
                      </Card>
                    </Col>
                  ))
                )}
              </Row>
            </Col>
          </Row>
          {/* Sec 6 */}
          <Row style={{ marginTop: "1rem" }}>
            <Col md={8}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Latest Reviews
              </h2>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "grey",
                    fontWeight: "400",
                  }}
                >
                  So you can make better purchase
                </h3>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "deeppink",
                    fontWeight: "400",
                  }}
                >
                  See more &#x3e;
                </h3>
              </div>
              <Row>
                <Slider {...settingsSlider}>
                  {review?.length === 0 ? (
                    <div></div>
                  ) : (
                    review?.map((e) => (
                      <Col style={{ padding: "1rem" }} key={e.id}>
                        <Card>
                          <Card.Header>
                            <Row style={{ marginTop: "1rem" }}>
                              <Col md={3}>
                                <Image
                                  className={styles.ImageArtc}
                                  alt="article"
                                  width={35}
                                  height={35}
                                  src={e.product.image}
                                />
                              </Col>
                              <Col>
                                <p
                                  style={{
                                    fontSize: "14px",
                                    fontWeight: "700",
                                    marginBottom: "0",
                                    color: "#000",
                                  }}
                                >
                                  {e.product.name}
                                </p>
                                <p
                                  style={{
                                    fontSize: "14px",
                                    fontWeight: "400",
                                    color: "#000",
                                  }}
                                >
                                  {e.product.desc}
                                </p>
                              </Col>
                            </Row>
                          </Card.Header>
                          <Card.Body>
                            <p
                              style={{
                                fontSize: "14px",
                                fontWeight: "700",
                                marginBottom: "0",
                                color: "#000",
                              }}
                            >
                              Rating: {e.star}
                            </p>
                            <p
                              style={{
                                fontSize: "14px",
                                fontWeight: "400",
                                color: "#000",
                                textAlign: "left",
                              }}
                            >
                              {e.comment}
                            </p>
                          </Card.Body>
                        </Card>
                        <Row style={{ position: "relative", top: "-20px" }}>
                          <Col
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                            md={12}
                          >
                            <Image
                              // className={styles.ImageArtc}
                              alt="article"
                              width={35}
                              height={35}
                              src={e.product.image}
                            />
                          </Col>
                          <Col
                            md={12}
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                          >
                            <p
                              style={{
                                fontSize: "14px",
                                fontWeight: "500",
                                color: "#000",
                                textAlign: "left",
                                marginBottom: "0",
                              }}
                            >
                              {e.user}
                            </p>
                          </Col>
                          <Col
                            md={12}
                            style={{
                              display: "flex",
                              justifyContent: "center",
                            }}
                          >
                            <p
                              style={{
                                fontSize: "14px",
                                fontWeight: "400",
                                color: "grey",
                                textAlign: "left",
                              }}
                            >
                              Combination Skin
                            </p>
                          </Col>
                        </Row>
                      </Col>
                    ))
                  )}
                </Slider>
              </Row>
            </Col>
            <Col
              md={4}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div style={{ padding: "5rem" }}>
                <div
                  style={{
                    background: "lightgrey",
                    color: "#00000",
                    border: "1px solid #000000",
                    width: "300px",
                    height: "250px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    fontSize: "28px",
                    textAlign: "center",
                  }}
                >
                  <p style={{ textAlign: "center" }}>
                    MR2 <br></br> 300x250
                  </p>
                </div>
              </div>
            </Col>
          </Row>
          {/* Sec 7 */}
          <Row>
            <Col md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Popular Groups
              </h2>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "grey",
                    fontWeight: "400",
                  }}
                >
                  Where the beauty TALK are
                </h3>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "deeppink",
                    fontWeight: "400",
                  }}
                >
                  See more &#x3e;
                </h3>
              </div>
            </Col>
            <Col md={12}>
              <Row>
                <Col md={3}>
                  <Card style={{ boxShadow: "10px 10px 5px #888888" }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        padding: "1rem",
                      }}
                    >
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS7}
                      />
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "24px",
                          color: "#000000",
                          fontWeight: "700",
                          textAlign: "center",
                        }}
                      >
                        Embrace the Curl
                      </p>
                    </div>
                    <div>
                      <Row>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            F
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            D
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            !
                          </p>
                        </Col>
                      </Row>
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          color: "#000000",
                          fontWeight: "500",
                          textAlign: "center",
                        }}
                      >
                        May our curls pop and never stop!
                      </p>
                    </div>
                  </Card>
                </Col>
                <Col md={3}>
                  <Card style={{ boxShadow: "10px 10px 5px #888888" }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        padding: "1rem",
                      }}
                    >
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS7}
                      />
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "24px",
                          color: "#000000",
                          fontWeight: "700",
                          textAlign: "center",
                        }}
                      >
                        Embrace the Curl
                      </p>
                    </div>
                    <div>
                      <Row>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            F
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            D
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            !
                          </p>
                        </Col>
                      </Row>
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          color: "#000000",
                          fontWeight: "500",
                          textAlign: "center",
                        }}
                      >
                        May our curls pop and never stop!
                      </p>
                    </div>
                  </Card>
                </Col>
                <Col md={3}>
                  <Card style={{ boxShadow: "10px 10px 5px #888888" }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        padding: "1rem",
                      }}
                    >
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS7}
                      />
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "24px",
                          color: "#000000",
                          fontWeight: "700",
                          textAlign: "center",
                        }}
                      >
                        Embrace the Curl
                      </p>
                    </div>
                    <div>
                      <Row>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            F
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            D
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            !
                          </p>
                        </Col>
                      </Row>
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          color: "#000000",
                          fontWeight: "500",
                          textAlign: "center",
                        }}
                      >
                        May our curls pop and never stop!
                      </p>
                    </div>
                  </Card>
                </Col>
                <Col md={3}>
                  <Card style={{ boxShadow: "10px 10px 5px #888888" }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        padding: "1rem",
                      }}
                    >
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS7}
                      />
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "24px",
                          color: "#000000",
                          fontWeight: "700",
                          textAlign: "center",
                        }}
                      >
                        Embrace the Curl
                      </p>
                    </div>
                    <div>
                      <Row>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            F
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            D
                          </p>
                        </Col>
                        <Col>
                          <p
                            style={{
                              fontSize: "24px",
                              color: "#000000",
                              fontWeight: "700",
                              textAlign: "center",
                            }}
                          >
                            !
                          </p>
                        </Col>
                      </Row>
                    </div>
                    <div>
                      <p
                        style={{
                          fontSize: "18px",
                          color: "#000000",
                          fontWeight: "500",
                          textAlign: "center",
                        }}
                      >
                        May our curls pop and never stop!
                      </p>
                    </div>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* SEC 8 */}
          <Row style={{ padding: "3rem 0" }}>
            <Col md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Latest Videos
              </h2>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "grey",
                    fontWeight: "400",
                  }}
                >
                  Watch and learn, ladies
                </h3>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "deeppink",
                    fontWeight: "400",
                  }}
                >
                  See more &#x3e;
                </h3>
              </div>
            </Col>
            <Col md={12}>
              <Row>
                <Col style={{ marginTop: "1rem" }} md={8}>
                  <Image
                    src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                    alt="fd task"
                    width={100}
                    height={100}
                    className={styles.imgS3}
                  />
                </Col>
                <Col style={{ marginTop: "1rem" }} md={4}>
                  <Row>
                    <Col md={12} style={{ marginBottom: "0.8rem" }}>
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS3}
                      />
                    </Col>
                    <Col md={12}>
                      <Image
                        src="https://femaledaily.com/_next/image?url=https%3A%2F%2Feditorial.femaledaily.com%2Fwp-content%2Fuploads%2F2023%2F03%2Fpet-friendly.png&w=1920&q=100"
                        alt="fd task"
                        width={100}
                        height={100}
                        className={styles.imgS3}
                      />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
          </Row>
          {/* SEC 9 */}
          <Row>
            <Col md={12}>
              <h2
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Top Brands
              </h2>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "grey",
                    fontWeight: "400",
                  }}
                >
                  We All Know and love
                </h3>
                <h3
                  style={{
                    fontSize: "18px",
                    color: "deeppink",
                    fontWeight: "400",
                  }}
                >
                  See more &#x3e;
                </h3>
              </div>
            </Col>
            <Col md={12}>
              <Row>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
                <Col
                  md={2}
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Image
                    alt=" Logo"
                    src={
                      "https://s3.ap-southeast-1.amazonaws.com/assets.femaledaily.com/web-assets/icon/ic-fd.svg"
                    }
                    width={100}
                    height={50}
                    quality="100%"
                    priority={true}
                    className={styles.imgSec3}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
          {/* Sec 10 */}
          <Row style={{ padding: "1.5rem 0" }}>
            <Col md={12}>
              <p
                style={{
                  fontSize: "24px",
                  color: "#000000",
                  fontWeight: "700",
                }}
              >
                Female Daily - Find everything you want to know about beauty and
                Female Daily
              </p>
            </Col>
            <Col md={12}>
              <p
                style={{
                  fontSize: "18px",
                  color: "#000000",
                  fontWeight: "500",
                }}
              >
                Product Review, Tips & Trick, Expert and Customer Opinions,
                Beauty Tutorial, Discussions, Beauty Workshop, anything! We are
                here to be your friendy solution to all things beauty, inside
                and outside!
              </p>
            </Col>
          </Row>
        </Container>
      </main>
    </>
  );
}

export const getStaticProps = async () => {
  const res = await fetch(
    "https://virtserver.swaggerhub.com/hqms/FDN-WP/0.1/wp"
  );
  const data = await res.json();
  console.log(data);

  return {
    props: {
      editor: data["editor's choice"],
      article: data["latest articles"],
      review: data["latest review"],
    },
    revalidate: 10,
  };
};